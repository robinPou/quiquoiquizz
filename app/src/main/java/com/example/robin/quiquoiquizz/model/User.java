package com.example.robin.quiquoiquizz.model;

public class User {

    private String pseudo;

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        pseudo = pseudo;
    }

    @Override
    public String toString() {
        return "User{" +
                "pseudo='" + pseudo + '\'' +
                '}';
    }
}
