package com.example.robin.quiquoiquizz.controller;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.robin.quiquoiquizz.R;

import org.w3c.dom.Text;

import java.util.List;

public class ViewQuestion extends AppCompatActivity {

    public static final String RESULTAT_QUESTION = "com.example.robin.quiquoiquizz.RESULTAT";

    CountDownTimer cd;
    int reponseValide;
    long duree;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

        Intent intent = getIntent();
        Bundle question = intent.getBundleExtra(Game.VIEW_QUESTION);

        TextView intitule = findViewById(R.id.questionText);

        reponseValide = question.getInt("indexReponse");
        intitule.setText(question.getCharSequence("intitule"));
        Bundle reponses = question.getBundle("reponses");

        Button reponse1 = findViewById(R.id.button1);
        Button reponse2 = findViewById(R.id.button2);
        Button reponse3 = findViewById(R.id.button3);
        Button reponse4 = findViewById(R.id.button4);

        reponse1.setTag(0);
        reponse2.setTag(1);
        reponse3.setTag(2);
        reponse4.setTag(3);

        reponse1.setText(reponses.getCharSequence("reponse0"));
        reponse2.setText(reponses.getCharSequence("reponse1"));
        reponse3.setText(reponses.getCharSequence("reponse2"));
        reponse4.setText(reponses.getCharSequence("reponse3"));

        duree = question.getLong("duree");


        cd = new CountDownTimer(duree * 1000, 1000) {
            @Override
            public void onTick(long l) {
                // TODO: Modifier le champs pour le timer dans l'activite
                TextView timer = findViewById(R.id.timerText);
                timer.setText(Long.toString(l/1000));
            }

            @Override
            public void onFinish() {
                Intent intent = new Intent();
                intent.putExtra(RESULTAT_QUESTION, 0);
                setResult(RESULT_OK,intent);
                finish();
            }
        }.start();
    }

    public void choixReponse(View view) {
        cd.cancel();

        Intent intent = new Intent();
        int index = (int) view.getTag();
        if (index == reponseValide) {
            intent.putExtra(RESULTAT_QUESTION, 1);
        } else {
            intent.putExtra(RESULTAT_QUESTION, 0);
        }
        setResult(RESULT_OK,intent);
        finish();
    }
}
