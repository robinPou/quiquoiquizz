package com.example.robin.quiquoiquizz.model;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class BanqueQuestion {


    private List<Question> listeQuestion;
    private int questionSuivanteIndex;

    public BanqueQuestion(List <Question> questionListe){
            listeQuestion = questionListe;
            Collections.shuffle(listeQuestion);
            questionSuivanteIndex = 0;
    }

    public Question getQuestion(){

        if (questionSuivanteIndex == listeQuestion.size()) questionSuivanteIndex = 0;
        return listeQuestion.get(questionSuivanteIndex++);
    }


}
