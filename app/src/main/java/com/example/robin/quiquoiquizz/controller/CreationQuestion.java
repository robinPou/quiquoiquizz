package com.example.robin.quiquoiquizz.controller;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.robin.quiquoiquizz.R;
import com.example.robin.quiquoiquizz.model.Question;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class CreationQuestion extends AppCompatActivity {

    private DatabaseReference databaseQuestion;
    private static final String TAG = "AJOUTER EN BASE";

    private EditText editText_libelleQuestion;
    private EditText editText_réponse1;
    private EditText editText_réponse2;
    private EditText editText_réponse3;
    private EditText editText_réponse4;
    private EditText editText_choixRéponse;
    private EditText editText_timer;

    Button button_CréationQuestion;

    private String libelle;
    private  List<String> listeRéponse = new ArrayList<>();

    private int choixRéponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_creation_question);


        String url = "https://quiquoiquizz.firebaseio.com";
        databaseQuestion = FirebaseDatabase.getInstance().getReferenceFromUrl(url);


    editText_libelleQuestion =  (EditText) findViewById(R.id.editText_libelleQuestion);

    editText_réponse1 = (EditText) findViewById(R.id.editText_réponse1);
    editText_réponse2 = (EditText) findViewById(R.id.editText_réponse2);
    editText_réponse3 = (EditText) findViewById(R.id.editText_réponse3);
    editText_réponse4 = (EditText) findViewById(R.id.editText_réponse4);

    editText_choixRéponse =  (EditText) findViewById(R.id.editText_choixRéponse);
    editText_timer = (EditText) findViewById(R.id.editText_timer);
    button_CréationQuestion = (Button) findViewById(R.id.button_CréationQuestion);

        button_CréationQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addQuestion();
            }
        });
    }

    /**
     *  ajoute la question du formulaire sur firebase
     */
    private void addQuestion(){

        String rép1 = editText_réponse1.getText().toString();
        String rép2 = editText_réponse2.getText().toString();
        String rép3 = editText_réponse3.getText().toString();
        String rép4 = editText_réponse4.getText().toString();

        listeRéponse.add(rép1);
        listeRéponse.add(rép2);
        listeRéponse.add(rép3);
        listeRéponse.add(rép4);

        libelle = editText_libelleQuestion.getText().toString();
        int choixReponse = Integer.parseInt(editText_choixRéponse.getText().toString());
        choixReponse = choixReponse - 1;

        int time = Integer.parseInt(editText_timer.getText().toString());

        if (choixReponse > 3  || choixReponse <0){
            Toast.makeText(this, "mauvaise valeur ! ", Toast.LENGTH_SHORT).show();
        }
        else if
        (!libelle.equals("")&& !rép1.equals("") && !rép2.equals("") && !rép3.equals("") && !rép4.equals("") ){

            String id = databaseQuestion.push().getKey();
            Question question = new Question(libelle,listeRéponse,choixReponse,time);
            databaseQuestion.child(id).setValue(question);

            Toast.makeText(this," ViewQuestion ajoutée !", Toast.LENGTH_SHORT).show();
            finish();

        }else
        {
            Toast.makeText(this, "champs vide ! ", Toast.LENGTH_SHORT).show();
        }

    }
}
