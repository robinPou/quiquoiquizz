package com.example.robin.quiquoiquizz.model;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.List;

@IgnoreExtraProperties
public class Question {

    private String key;
    //private String questionId;
    private String intitule;
    private List<String> choixListe;
    private int reponseIndex;
    private long time;

    public Question() {

    }

    public Question(String intitule, List<String> choixListe, int reponseIndex, int time) {
        // this.questionId = questionId;
        this.intitule = intitule;
        this.choixListe = choixListe;
        this.reponseIndex = reponseIndex;
        this.time = time;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public List<String> getChoixListe() {
        return choixListe;
    }

    public void setChoixListe(List<String> choixListe) {
        if (choixListe == null) {
            throw new IllegalArgumentException("cannot be null");
        }

        this.choixListe = choixListe;
    }

    public int getReponseIndex() {
        return reponseIndex;
    }

    public void setReponseIndex(int reponseIndex) {
        if (reponseIndex < 0 || reponseIndex >= choixListe.size()) {
            throw new IllegalArgumentException("erreur Index réponse");
        }

        this.reponseIndex = reponseIndex;
    }

    @Override
    public String toString() {
        return "ViewQuestion{" +
                "intitulé question='" + intitule + '\'' +
                ", choix de la liste =" + choixListe +
                ", Index de la question =" + reponseIndex +
                '}';
    }


    public long getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public static Question mapFromObject(Object object) {
        return null;
    }
}
