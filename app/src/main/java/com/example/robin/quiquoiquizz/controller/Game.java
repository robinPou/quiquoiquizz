package com.example.robin.quiquoiquizz.controller;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.example.robin.quiquoiquizz.R;
import com.example.robin.quiquoiquizz.model.BanqueQuestion;
import com.example.robin.quiquoiquizz.model.Question;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class Game extends AppCompatActivity implements View.OnClickListener {


    public static final String VIEW_QUESTION = "com.example.robin.quiquoiquizz.QUESTION";


    private DatabaseReference databaseQuestion;

    private int gameScore;

    private BanqueQuestion banqueQuestion;

    private int nbQuestions;

    public static final String BUNDLE_SCORE = "BUNDLE_SCORE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        databaseQuestion = FirebaseDatabase.getInstance().getReference();
        nbQuestions = 5;

        databaseQuestion.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Question> questions = new ArrayList<>();
                for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                    Question question = dataSnapshot.child(childDataSnapshot.getKey()).getValue(Question.class);

                    questions.add(question);
                }

                banqueQuestion = new BanqueQuestion(questions);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println(databaseError);
            }
        });
    }

    @Override
    public void onClick(View view) {

    }

    public void startQuestion(View view) {

        Question q = banqueQuestion.getQuestion();
        Intent intent = new Intent(this, ViewQuestion.class);
        Bundle question = new Bundle();
        question.putCharSequence("intitule", q.getIntitule());
        question.putLong("duree", q.getTime());
        Bundle reponses = new Bundle();
        for (int i = 0; i < q.getChoixListe().size(); i++) {
            reponses.putCharSequence("reponse"+ Integer.toString(i), q.getChoixListe().get(i));
        }
        question.putBundle("reponses", reponses);
        question.putInt("indexReponse",q.getReponseIndex());
        intent.putExtra(VIEW_QUESTION, question);
        startActivityForResult(intent,1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == RESULT_OK) {
            int result = data.getIntExtra(ViewQuestion.RESULTAT_QUESTION, 0);
            TextView tv = findViewById(R.id.textView);

            if (result == 1) {
                tv.setText("Bonne réponse !");
                gameScore++;
            } else {
                tv.setText("Mauvaise réponse !");
            }
            if (--nbQuestions == 0) {
                endGame();
            }
        }
    }

    private void endGame() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Fin du jeu !")
                .setMessage("Ton score est de " + gameScore)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Intent intent = new Intent();
                        intent.putExtra(BUNDLE_SCORE, gameScore);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                })
                .setCancelable(false)
                .create()
                .show();
    }

    @Override
    protected void onStart() {
        super.onStart();

        System.out.println("Game::onStart()");
    }

    @Override
    protected void onResume() {
        super.onResume();

        System.out.println("Game::onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();

        System.out.println("Game::onPause()");
    }

    @Override
    protected void onStop() {
        super.onStop();

        System.out.println("Game::onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        System.out.println("Game::onDestroy()");
    }


}
