package com.example.robin.quiquoiquizz.model;

import java.util.ArrayList;

public class Quizz {
    private ArrayList<Question> questions;
    private String title;

    public Quizz(String title) {
        this.title = title;
    }

    public void setQuestions(ArrayList<Question> questions) {
        this.questions = questions;
    }

    public ArrayList<Question> getQuestions() {
        return questions;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
