package com.example.robin.quiquoiquizz.controller;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.robin.quiquoiquizz.R;
import com.example.robin.quiquoiquizz.model.User;

import static java.lang.System.out;

public class Acceuil extends AppCompatActivity {

    private SharedPreferences preferences;

    public static final int ACCEUIL_REQUEST_CODE = 0;

    public static final  String PREF_KEY_SCORE = "PREF_KEY_SCORE";
    public static final String PREF_KEY_PSEUDO = "PREF_KEY_PSEUDO";

    private TextView textViewAcceuil;
    private TextView activity_acceuil_TextViewBienvenue;
    private EditText activity_acceuil_EditText;
    private Button btnGame;
    private Button btnCrea;
    private User user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acceuil);

        user = new User();

        preferences =  getPreferences(MODE_PRIVATE);

        textViewAcceuil =  findViewById(R.id.activity_acceuil_txt);
        activity_acceuil_EditText = findViewById(R.id.activity_acceuil_EditText);

        btnGame = (Button) findViewById(R.id.activity_acceuil_btn);
        btnGame.setEnabled(false);

        btnCrea = findViewById(R.id.activity_acceuil_btnCrea);


        activity_acceuil_EditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                btnGame.setEnabled(s.toString().length()!=0);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btnCrea.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent intentCrea = new Intent(Acceuil.this, CreationQuestion.class);
                startActivity(intentCrea);
                                       }
        });

        btnGame.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String pseudo = activity_acceuil_EditText.getText().toString();
                user.setPseudo(pseudo);

                preferences.edit().putString(PREF_KEY_PSEUDO, user.getPseudo()).apply();


                Intent gameIntent = new Intent(Acceuil.this, Game.class);
                startActivityForResult(gameIntent, ACCEUIL_REQUEST_CODE );
            }
        });

        }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (ACCEUIL_REQUEST_CODE == requestCode && RESULT_OK == resultCode) {

            int score = data.getIntExtra(Game.BUNDLE_SCORE, 0);
            preferences.edit().putInt(PREF_KEY_SCORE,score).apply();

           acceuillirJoueur();
        }
    }

    private void acceuillirJoueur (){

        String pseudo = preferences.getString(PREF_KEY_PSEUDO, null);
        if(null != pseudo){

            int score = preferences.getInt(PREF_KEY_SCORE, 0);

            String texteAcceuil = " Bon retour !   ton dernier score est de " + score ;
            textViewAcceuil.setText(texteAcceuil);
            activity_acceuil_EditText.setText(pseudo);
            activity_acceuil_EditText.setSelection(pseudo.length());
            btnGame.setEnabled(true);

        }
    }


    @Override
    protected void onStart() {
        super.onStart();

        out.println("Acceuil::onStart()");
    }

    @Override
    protected void onResume() {
        super.onResume();

        out.println("Acceuil::onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();

        out.println("Acceuil::onPause()");
    }

    @Override
    protected void onStop() {
        super.onStop();

        out.println("Acceuil::onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        out.println("Acceuil::onDestroy()");
    }

}
